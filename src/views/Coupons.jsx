import DataTable from '../components/DataTable';
import Select from '../components/Select';
import {useEffect, useState} from 'react';
import Axios from 'axios';


function Coupons(){

    const [rows, setRows] = useState([]);
    const [rowsFilter, setRowsFilter] = useState([]);

    const cols = [
        {field: 'idcupon', headerName: 'ID'},
        {field: 'codigocupon', headerName: 'Código'},
        {field: 'porcentaje', headerName: 'Porcentaje de descuento'},
        {field: 'fechadesde', headerName: 'Válido desde'},
        {field: 'fechahasta', headerName: 'Válido hasta'},
        {field: 'nombrestatus', headerName: 'Estado'},
    ];

    useEffect(() =>{
        getData();
    },[])

    async function getData(){
        const responseApi = await Axios.post('http://35.188.155.248/bk_getCupones')
            .then((response)=> {
                return response.data.resultado;
            });
        setRows(responseApi);
        setRowsFilter(responseApi);
    }

    const [estado, setEstado] = useState('')

    function changeSelect(e){
        setEstado(e.target.value)
        const oldRows = rowsFilter;
        const nRows = [];
        

        oldRows.map(row => {
            if (e.target.value === row.nombrestatus){
                return nRows.push(row);
            }
            else if(e.target.value === ''){
                return nRows.push(row);
            }
        })

        setRows(nRows)        
    }

    return (
        <div class="container">
            <h1>Cuponera</h1>
            <div className="col s9">
                <Select value={estado} onChange={changeSelect}/>
                <DataTable columns={cols} rows={rows}/>
            </div>
        </div>
    )

}

export default Coupons;