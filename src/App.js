import Coupons from './views/Coupons'
import {Router, Switch, Route} from 'react-router-dom';
import {createBrowserHistory} from 'history'
import './App.css';
import 'materialize-css/dist/css/materialize.min.css';

//Agregamos el Browser history para el historial de rutas 
let bHistory = createBrowserHistory();

function App() {
  return (
    <Router history={bHistory}>
      <Switch>
        <Route exact path="/" component={Coupons}></Route>
      </Switch>

    </Router>
  )
}

export default App;
