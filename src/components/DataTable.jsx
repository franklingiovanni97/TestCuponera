import { useEffect, useState } from 'react';

function DataTable(data) {
    const [rows, setRows] = useState(data.rows)
    const [search, setSearch] = useState("")
   useEffect(() =>{
        setRows(data.rows)
    },[data.rows])

    function handlerSearch(event){
        
        const test = data.rows.filter(row => {
            return row.codigocupon.toLowerCase().includes(event.target.value.toLowerCase());
        });
        setRows(test)
        setSearch(event.target.value);

    }

    return (
        <div className="row">
            <input
                onChange={e => {
                    handlerSearch(e);
                }}
                type="text"
                value={search}
                placeholder= "Filtro por código"
                
            />




            <table>
                <thead>
                    <tr>
                        {
                            data.columns.map((column, keyColumn) => {
                                
                                return (
                                    <th key={keyColumn}>{column.headerName}</th>
                                )
                            })
                        }
                    </tr>
                </thead>

                <tbody>
                    {
                        
                        rows.map((row, keyRow) => {
                            return (
                                <tr key={keyRow}>
                                    {
                                        data.columns.map((column, keyColumn) => {
                                            return (
                                                <td key={keyRow + '-' + keyColumn}>{row[column.field]}</td>
                                            )
                                        })
                                    }
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </div>
    );
}

export default DataTable;