import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import { useEffect, useState } from 'react';

function Select(props) {
    const [valueSelect, setValueSelect] = useState('');



    useEffect(() => {
        setValueSelect(props.value)
    }, [props.value]);

    return (
        <div>
            <FormControl>                
                <NativeSelect id="select" name="Estado" value={valueSelect} onChange={props.onChange}>
                    <option value="">Todos los registros</option>
                    <option value="Activo">Activo</option>
                    <option value="Inactivo">Inactivo</option>
                </NativeSelect>
            </FormControl>
        </div>



    )
}

export default Select;